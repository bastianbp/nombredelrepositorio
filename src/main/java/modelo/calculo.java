/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;


public class calculo {

     private double capital, anos, tasaanual, interes;

    public calculo() {        
    } 
    
    public double getCapital() {
        return capital;
    }

    public void setCapital(double capital) {
        this.capital = capital;
    }

    public double getTasaanual() {
        return tasaanual;
    }

    public void setTasaanual(double tasaanual) {
        this.tasaanual = tasaanual;
    }

    public double getAnos() {
        return anos;
    }

    public void setAnos(double anos) {
        this.anos = anos;
    }

    public double getInteres() {
        return interes;
    }

    public void setInteres(double interes) {
        this.interes = interes;
    }
    
    public void operacion (double valorcapital,double porcentajetasa,double anos){
    
        double Porcentajetasa = (porcentajetasa/100);
        double finteres = (valorcapital * Porcentajetasa *anos );
        setInteres(finteres);
        setCapital(valorcapital);
        setTasaanual(Porcentajetasa);
        setAnos(anos);
                      
    }

    
    
}
